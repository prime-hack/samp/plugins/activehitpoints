#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent.h>
#include <SRHook.hpp>

class AsiPlugin : public SRDescent {
	SRHook::Hook<float>				   setHP{ 0x13845, 5, "samp" };
	SRHook::Hook<class CPed *, size_t> compDmgIn{ 0x4B5AC0, 6 };
	SRHook::Hook<>					   compDmgOut{ 0x4B5CE2, 7 };

	size_t		dmg_resp = 0;
	class CPed *ped		 = nullptr;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();
};

#endif // MAIN_H
