#include "loader.h"
#include "../main.h"
#include <d3d9/proxydirectx.h>
#include <regex>
#include <SRHook.hpp>

std::string_view PROJECT_NAME = _PRODJECT_NAME;
stGlobalHandles g_handle;
stGlobalClasses g_class;
stGlobalPVars g_vars;

static AsiPlugin *pAsiPlugin = static_cast<AsiPlugin*>(nullptr);

bool InstallD3DHook() {
	static bool isDxHooked = false;
	auto device = *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 );
	if ( !device ) return false;
	if ( isDxHooked ) return true;
	isDxHooked = true;
	g_class.DirectX = new hookIDirect3DDevice9(device);
	*reinterpret_cast<IDirect3DDevice9**>(0xC97C28) =
			dynamic_cast<IDirect3DDevice9*>(g_class.DirectX);
	return true;
}

CALLHOOK GameLoop()
{
	g_class.cursor = SRCursor::Instance();
	g_handle.d3d9 = GetModuleHandleA("d3d9.dll");
	if (g_handle.d3d9 == nullptr || g_handle.d3d9 == reinterpret_cast<HANDLE>(-1))
		return;
	if ( !InstallD3DHook() ) return;

	static bool hooked = false;
	if (hooked)
		return;
	hooked = true;

	g_class.events = SREvents::Instance();
	pAsiPlugin = new AsiPlugin();
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID)
{
	static SRHook::Hook<> gameloopHook{0x748DA3, 6};

	if (dwReasonForCall == DLL_PROCESS_ATTACH){
		g_handle.plugin = hModule;

		char name[256];
		GetModuleFileNameA(hModule, name, 256);
		std::cmatch m;
		std::regex re(R"((.*\\)((.*).asi))", std::regex::icase);
		if ( std::regex_match( name, m, re ) ) {
			static auto PLUGIN_PATH = m[1].str();
			static auto PLUGIN_NAME = m[3].str();

			g_vars.pluginPath = PLUGIN_PATH;
			g_vars.pluginName = PLUGIN_NAME;
		}

		gameloopHook.install();
		gameloopHook.onBefore += GameLoop;
	}
	else if (dwReasonForCall == DLL_THREAD_ATTACH){
		if (g_class.events)
			g_class.events->Inject();
	}
	else if (dwReasonForCall == DLL_THREAD_DETACH){
		if (g_class.events)
			g_class.events->Deinject();
	}
	else if (dwReasonForCall == DLL_PROCESS_DETACH){
		gameloopHook.remove();
		delete pAsiPlugin;
		pAsiPlugin = nullptr;
		g_class.cursor->DeleteInstance();
		g_class.events->DeleteInstance();
		if ( g_class.DirectX->d3d9_destroy() ) delete g_class.DirectX;
	}

	return TRUE;
}

int MessageBox(std::string_view text, std::string_view title, UINT type)
{
	return MessageBoxA(g_vars.hwnd, text.data(), title.data(), type);
}
