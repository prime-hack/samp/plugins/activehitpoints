#include "main.h"
#include <gtasa.hpp>
#include <samp_pimpl.hpp>

AsiPlugin::AsiPlugin() : SRDescent() {
	// Вместо 1000 ХП впихиваем реальный ХП
	if ( SAMP::isR3() ) setHP.changeAddr( 0x16A3E );
	setHP.install();
	setHP.onBefore += []( SRHook::CPU &cpu, float &hp ) {
		hp = *(float *)( cpu.EBP + ( SAMP::isR1() ? 0x1BC : 0x1B0 ) );
		if ( hp <= 0.0f ) hp = 0.1f;
	};

	// Не убиваем игрока локально (то, чего видимо хотел достичь калкор устанавливая 1000хп)
	compDmgIn.install( 4 );
	compDmgIn.onBefore += [this]( CPed *&ped, size_t dmg_resp ) {
		if ( ped != LOCAL_PLAYER ) {
			this->ped	   = ped;
			this->dmg_resp = dmg_resp;
		} else {
			this->ped	   = nullptr;
			this->dmg_resp = 0;
		}
	};
	compDmgOut.install();
	compDmgOut.onBefore += [this] {
		if ( !ped || !dmg_resp ) return;
		if ( ped->hitpoints <= 0.0f ) ped->hitpoints = 0.1f; // восстанавливаем чутка хп, если оно кончилось
		*(bool *)( dmg_resp + 9 ) = false; // не убивать игрока принудительно
	};
}

AsiPlugin::~AsiPlugin() {}
